import { combineReducers } from 'redux';

const data = (state = {}, action) => {
    switch (action.type) {
        case "FETCH_PELUQUERIA_SUCCESS":
        case "EDIT_PELUQUERIA_SUCCESS":
            return action.response;
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case "FETCH_PELUQUERIA_REQUEST":
            return true;
        case "FETCH_PELUQUERIA_SUCCESS":
        case "FETCH_PELUQUERIA_FAILURE":
            return false;
        default:
            return state;
    }
};

const isEditing = (state = false, action) => {
    switch (action.type) {
        case "EDIT_PELUQUERIA_REQUEST":
            return true;
        case "EDIT_PELUQUERIA_SUCCESS":
        case "EDIT_PELUQUERIA_FAILURE":
            return false;
        default:
            return state;
    }
};

const isAdding = (state = false, action) => {
    switch (action.type) {
        case "ADD_PELUQUERIA_REQUEST":
            return true;
        case "ADD_PELUQUERIA_SUCCESS":
        case "ADD_PELUQUERIA_FAILURE":
            return false;
        default:
            return state;
    }
};

const peluqueria = combineReducers({
    data,
    isFetching,
    isEditing,
    isAdding
});

export default peluqueria;

export const getDataPeluqueria = (state) =>
    state.peluqueria.peluqueria.data;

export const getIsFetching = (state) =>
    state.peluqueria.peluqueria.isFetching;

export const getIsEditing = (state) =>
    state.peluqueria.peluqueria.isEditing;

export const getIsAdding = (state) =>
    state.peluqueria.peluqueria.isAdding;