import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { injectIntl } from 'react-intl';
import * as actions from '../../actions/productos';
import * as connectors from '../../reducers/productos/showModalEditProducto';
import messages from './../../i18n/messages';
import validate from './ProductoValidate';
import { formatMessage } from './../../i18n';
import ProductoModal from './ProductoModal';

class ProductoEdit extends Component {

    constructor(props) {
        super(props);
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
    }

    validateAndCreatePost(values) {
        const {editProducto, initialValues} = this.props;
        editProducto(initialValues.idProducto, values.nombre.trim(), values.precio, values.stock);
    }

    render() {
        const {showModalEditProducto, closeModalEditProducto} = this.props;
        return (
            <ProductoModal
                showModalProp={showModalEditProducto}
                closeModalAction={closeModalEditProducto}
                validateAndCreatePost={this.validateAndCreatePost}
                idModal="editProductoModal"
                titleModal={formatMessage(messages.editarProducto)}
                {...this.props}
                {...this.props.intl} />
        );
    }
}

const reduxFormConfig = {
    form: 'productoEditForm',
    validate,
    enableReinitialize: true
};

ProductoEdit = reduxForm(
    reduxFormConfig
)(ProductoEdit);

const mapStateToProps = (state) => ({
    showModalEditProducto: connectors.getOpen(state),
    initialValues: connectors.getProducto(state)
});

ProductoEdit = connect(
    mapStateToProps,
    actions
)(ProductoEdit);

export default injectIntl(ProductoEdit);