import { Schema, arrayOf } from 'normalizr';

export const service = new Schema('services', { idAttribute: 'idServicio' });
export const arrayOfServices = arrayOf(service);

export const producto = new Schema('productos', { idAttribute: 'idProducto' });
export const arrayOfProductos = arrayOf(producto);

export const cliente = new Schema('clientes', { idAttribute: 'idCliente' });
export const arrayOfClientes = arrayOf(cliente);

export const cita = new Schema('citas', { idAttribute: 'idCita' });
export const arrayOfCitas = arrayOf(cita);

export const factura = new Schema('facturas', { idAttribute: 'idFactura' });
export const arrayOfFacturas = arrayOf(factura);

export const peluqueria = new Schema('peluqueria', { idAttribute: 'idPeluqueria' });