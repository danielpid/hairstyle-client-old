import React from 'react';
import { Cell } from 'fixed-data-table';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Tooltip from 'react-bootstrap/lib/Tooltip';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';

const IconCell = ({rowIndex, dataListWrapper, icon, title, onClick}) => {
    const servicio = dataListWrapper.getObjectAt(rowIndex);
    const tooltip = (<Tooltip id="tooltip">{title}</Tooltip>);
    const iconCellStyle = {
        marginTop: '4px'
    }
    return (
        <OverlayTrigger placement="left" overlay={tooltip}>
            <Cell 
                style={{cursor: 'pointer'}}  
                onClick={() => onClick(servicio)}>
                <Glyphicon
                    glyph={icon}
                    style={iconCellStyle} 
                />
            </Cell>
        </OverlayTrigger>
    );
};

export default IconCell;