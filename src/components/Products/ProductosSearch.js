import React from 'react';
import { connect } from 'react-redux';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import messages from './../../i18n/messages';
import { formatMessage } from './../../i18n';
import { fetchProductosByCriteria } from '../../actions/productos';
import './ProductosSearch.css';

let ProductosSearch = ({dispatch}) => {

    function searchIfEnter(e, inputNombre, inputPrecio, inputStock) {
        if (e.keyCode === 13) {
            searchByCriteria(inputNombre, inputPrecio, inputStock);
        }
    }

    function searchByCriteria(inputNombre, inputPrecio, inputStock) {
        dispatch(fetchProductosByCriteria(
            inputNombre.value.trim(),
            inputPrecio ? inputPrecio.value : "",
            inputStock ? inputStock.value : ""));
    }

    function clean(inputNombre, inputPrecio, inputStock) {
        inputNombre.value = "";
        inputPrecio.value = "";
        inputStock.value = "";
    }

    let inputNombre, inputPrecio, inputStock;
    return (
        <Row bsClass="row-fluid" className="ProductosSearch">
            <Col md={4}>
                <input
                    ref={node => { inputNombre = node; }}
                    className="form-control"
                    type="text"
                    placeholder={formatMessage(messages.nombre)}
                    onKeyUp={e => searchIfEnter(e, inputNombre, inputPrecio, inputStock)}
                />
            </Col>
            <Col md={1}>
                <input
                    ref={node => { inputPrecio = node; }}
                    className="form-control"
                    type="number"
                    placeholder={formatMessage(messages.precio)}
                    onKeyUp={e => searchIfEnter(e, inputNombre, inputPrecio, inputStock)}
                />
            </Col>
            <Col md={1}>
                <input
                    ref={node => { inputStock = node; }}
                    className="form-control"
                    type="number"
                    placeholder={formatMessage(messages.stock)}
                    onKeyUp={e => searchIfEnter(e, inputNombre, inputPrecio, inputStock)}
                />
            </Col>
            <Col md={1}>
                <Button bsStyle="primary" type="button" 
                    onClick={() => searchByCriteria(inputNombre, inputPrecio, inputStock)} >
                    <Glyphicon glyph="search" /> {formatMessage(messages.buscar)}
                </Button>
            </Col>
            <Col md={1}>
                <Button type="button" 
                    onClick={() => clean(inputNombre, inputPrecio, inputStock)} >
                    <Glyphicon glyph="erase" /> {formatMessage(messages.limpiar)}
                </Button>
            </Col>
        </Row>
    );
};

ProductosSearch = connect()(ProductosSearch);

export default ProductosSearch;