import React from 'react';
import { connect } from 'react-redux';
import Modal from 'react-bootstrap/lib/Modal';
import ProgressBar from 'react-bootstrap/lib/ProgressBar';

let Wait = ({showModalWait}) => (
    <Modal id="waitModal" show={showModalWait.showing}>
        <Modal.Body>
            <h5>{showModalWait.msg}</h5>
            <ProgressBar active now={100} />
        </Modal.Body>
    </Modal>
);

const mapStateToProps = (state) => ({
    showModalWait: state.showModalWait
});

Wait = connect(
    mapStateToProps
)(Wait);

export default Wait;