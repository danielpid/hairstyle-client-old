import React from 'react';
import Image from 'react-bootstrap/lib/Image';
import './NotLoggedInContainer.css';

const NotLoggedInContainer = ({ children }) => 
    <div className="NotLoggedInContainer">
        <Image src="../../transparent-icon.png" rounded className="NotLoggedInContainer-image" />
        <p className="NotLoggedInContainer-name">HairStyleApp</p>
        <div className="NotLoggedInContainer-div">
            {children}
        </div>
    </div>

export default NotLoggedInContainer;