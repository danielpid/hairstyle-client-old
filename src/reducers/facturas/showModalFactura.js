const showModalFactura = (state = false, action) => {
    switch (action.type) {
        case 'OPEN_MODAL_FACTURA':
        case 'SET_FACTURA':
            return {
                open: true,
                factura: action.factura
            };
        case 'CLOSE_MODAL_FACTURA':
        case 'ADD_FACTURA_SUCCESS':
        case 'EDIT_FACTURA_SUCCESS':
            return {
                open: false,
                factura: null
            };
        default:
            return state;
    }
};

export default showModalFactura;

export const getOpen = (state) =>
    state.facturas.showModalFactura.open;

export const getFactura = (state) => {
    const factura = state.facturas.showModalFactura.factura;
    if (!factura) {
        return null;
    }
    const servicios = factura.detalle ?
        [...factura.detalle
            .filter(lineaFactura => lineaFactura.idServicio != null)
            .map(lineaFactura => ({
                idLineaFactura: lineaFactura.idLineaFactura,
                cantidad: lineaFactura.cantidad,
                precioUnidad: lineaFactura.precioUnidad,
                precio: lineaFactura.precioLineaFactura,
                servicio: {
                    idServicio: lineaFactura.idServicio,
                    nombre: lineaFactura.nombreServicio,
                    precio: lineaFactura.precioUnidad
                }
            })), {}]
        : [{}];
    const productos = factura.detalle ?
        [...factura.detalle
            .filter(lineaFactura => lineaFactura.idProducto != null)
            .map(lineaFactura => ({
                idLineaFactura: lineaFactura.idLineaFactura,
                cantidad: lineaFactura.cantidad,
                precioUnidad: lineaFactura.precioUnidad,
                precio: lineaFactura.precioLineaFactura,
                producto: {
                    idProducto: lineaFactura.idProducto,
                    nombre: lineaFactura.nombreProducto,
                    precio: lineaFactura.precioUnidad
                }
            })), {}]
        : [{}];
    return {
        idFactura: factura.idFactura,
        fechaAlta:factura.fechaAlta,
        cliente: {
            idCliente: factura.idCliente,
            nombre: factura.nombreCliente
        },
        servicios: servicios,
        productos: productos
    }
}