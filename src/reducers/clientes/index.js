import { combineReducers } from 'redux';
import clientes from './clientes';
import clientesDelete from './clientesDelete';
import showModalAddCliente from './showModalAddCliente';
import showModalEditCliente from './showModalEditCliente';

const clientesReducers = combineReducers({
    clientes,
    clientesDelete,
    showModalAddCliente,
    showModalEditCliente
});

export default clientesReducers;