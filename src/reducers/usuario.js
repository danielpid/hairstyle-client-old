export const isResettingPassword = (state = false, action) => {
    switch (action.type) {
        case "RESET_PASSWORD_REQUEST":
            return true;
        case "RESET_PASSWORD_SUCCESS":
        case "RESET_PASSWORD_FAILURE":
            return false;
        default:
            return state;
    }
}

export const getIsResettingPassword = (state) =>
    state.isResettingPassword;