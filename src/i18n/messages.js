import { defineMessages } from 'react-intl';

const messages = defineMessages({
    actualizando: {
        id: 'actualizando',
        defaultMessage: 'Actualizando...'
    },
    anterior: {
        id: 'anterior',
        defaultMessage: 'Anterior'
    },
    agenda: {
        id: 'agenda',
        defaultMessage: 'Agenda'
    },
    anadirCliente: {
        id: 'anadir.cliente',
        defaultMessage: 'Añadir cliente'
    },
    anadirProducto: {
        id: 'anadir.producto',
        defaultMessage: 'Añadir producto'
    },
    anadirServicio: {
        id: 'anadir.servicio',
        defaultMessage: 'Añadir servicio'
    },
    borrando: {
        id: 'borrando',
        defaultMessage: 'Borrando...'
    },
    buscando: {
        id: 'buscando',
        defaultMessage: 'Buscando...'
    },
    buscar: {
        id: 'buscar',
        defaultMessage: 'Buscar'
    },
    caja: {
        id: 'caja',
        defaultMessage: 'Caja'
    },
    cambiarPassword: {
        id: 'cambiar.password',
        defaultMessage: 'Cambiar contraseña'
    },
    cancelar: {
        id: 'cancelar',
        defaultMessage: 'Nombre'
    },
    cantidad: {
        id: 'cantidad',
        defaultMessage: 'Cantidad'
    },
    cantidadIncorrecta: {
        id: 'cantidad.incorrecta',
        defaultMessage: 'Cantidad incorrecta'
    },
    casa: {
        id: 'casa',
        defaultMessage: 'Casa'
    },
    cerrar: {
        id: 'cerrar',
        defaultMessage: 'Cerrar'
    },
    cerrarSesion: {
        id: 'cerrar.sesion',
        defaultMessage: 'Cerrar sesión'
    },
    cita: {
        id: 'cita',
        defaultMessage: 'Cita'
    },
    citaActualizada: {
        id: 'cita.actualizada',
        defaultMessage: 'Cita actualizada correctamente'
    },
    citaBorrada: {
        id: 'cita.borrada',
        defaultMessage: 'Cita borrada correctamente'
    },
    citaCreada: {
        id: 'cita.creada',
        defaultMessage: 'Cita creada correctamente'
    },
    cliente: {
        id: 'cliente',
        defaultMessage: '{num, plural, one {cliente} other {clientes}}'
    },
    clienteActualizado: {
        id: 'cliente.actualizado',
        defaultMessage: 'Cliente actualizado correctamente'
    },
    clienteBorrarValor: {
        id: 'cliente.borrar.valor',
        defaultMessage: 'Borrar cliente'
    },
    clienteCreado: {
        id: 'cliente.creado',
        defaultMessage: 'Cliente creado correctamente'
    },
    clienteNombre: {
        id: 'cliente.nombre',
        defaultMessage: 'Cliente'
    },
    clienteObligatorio: {
        id: 'cliente.obligatorio',
        defaultMessage: 'Introduzca un cliente'
    },
    clientes: {
        id: 'clientes',
        defaultMessage: 'Clientes'
    },
    clientesBorrado: {
        id: 'clientes.borrados',
        defaultMessage: 'Clientes borrados correctamente'
    },
    clientesNoBorrados: {
        id: 'clientes.no.borrados',
        defaultMessage: 'Los siguientes clientes no se pudieron borrar por tener citas o facturas: {nombresClientes}'
    },
    confirmarBorrado: {
        id: 'confirmar.borrado',
        defaultMessage: 'Confirmar borrado'
    },
    confirmarBorradoMensaje: {
        id: 'confirmar.borrado.mensaje',
        defaultMessage: 'Va a borrar {num} {elementos}, ¿desea continuar?'
    },
    crearCliente: {
        id: 'crear.cliente',
        defaultMessage: 'Crear cliente'
    },
    crearCuenta: {
        id: 'crear.cuenta',
        defaultMessage: 'Registrarse'
    },
    crearProducto: {
        id: 'crear.producto',
        defaultMessage: 'Crear producto'
    },
    crearServicio: {
        id: 'crear.servicio',
        defaultMessage: 'Crear servicio'
    },
    credencialesInvalidas: {
        id: 'credenciales.invalidas',
        defaultMessage: 'Correo electrónico o contraseña incorrecta'
    },
    descripcion: {
        id: 'descripcion',
        defaultMessage: 'Descripción'
    },
    dia: {
        id: 'dia',
        defaultMessage: 'Día'
    },
    diaObligatorio: {
        id: 'dia.obligatorio',
        defaultMessage: 'Día obligatorio'
    },
    editar: {
        id: 'editar',
        defaultMessage: 'Editar'
    },
    editarCliente: {
        id: 'editar.cliente',
        defaultMessage: 'Editar cliente'
    },
    editarProducto: {
        id: 'editar.producto',
        defaultMessage: 'Editar producto'
    },
    editarServicio: {
        id: 'editar.servicio',
        defaultMessage: 'Editar servicio'
    },
    eliminar: {
        id: 'eliminar',
        defaultMessage: 'Eliminar'
    },
    eliminarClientes: {
        id: 'eliminar.clientes',
        defaultMessage: 'Eliminar clientes'
    },
    eliminarFactura: {
        id: 'eliminar.factura',
        defaultMessage: 'la factura de {nombreCliente}'
    },
    eliminarProductos: {
        id: 'eliminar.productos',
        defaultMessage: 'Eliminar productos'
    },
    eliminarServicios: {
        id: 'eliminar.servicios',
        defaultMessage: 'Eliminar servicios'
    },
    email: {
        id: 'email',
        defaultMessage: 'Correo electrónico'
    },
    emailEnUso: {
        id: 'email.en.uso',
        defaultMessage: 'Correo electrónico ya registrado. Pruebe otro'
    },
    emailIncorrecto: {
        id: 'email.incorrecto',
        defaultMessage: 'Correo electrónico incorrecto',
    },
    emailObligatorio: {
        id: 'email.obligatorio',
        defaultMessage: 'Introduzca un correo electrónico de contacto'
    },
    emailRepita: {
        id: 'email.repita',
        defaultMessage: 'Confirme el correo electrónico'
    },
    emailRepitaDiferente: {
        id: 'email.repita.diferente',
        defaultMessage: 'Las direcciones de correo electrónico no coinciden'
    },
    emailRepitaObligatorio: {
        id: 'email.repita.obligatorio',
        defaultMessage: 'Introduzca el correo electrónico de nuevo'
    },
    emailResetPasswordSent: {
        id: 'email.reset.password.sent',
        defaultMessage: 'El correo electrónico para restablecer la contraseña se envió correctamente'
    },
    entrar: {
        id: 'entrar',
        defaultMessage: 'Entrar'
    },
    enviando: {
        id: 'enviando',
        defaultMessage: 'Enviando'
    },
    enviar: {
        id: 'enviar',
        defaultMessage: 'Enviar'
    },
    errorGenerico: {
        id: 'error.generico',
        defaultMessage: 'Se ha producido un error'
    },
    factura: {
        id: 'factura',
        defaultMessage: 'Factura'
    },
    facturaActualizada: {
        id: 'factura.actualizada',
        defaultMessage: 'Producto actualizado correctamente'
    },
    facturaBorrada: {
        id: 'factura.borrada',
        defaultMessage: 'Factura borrada correctamente'
    },
    facturaCreada: {
        id: 'factura.creada',
        defaultMessage: 'Factura creada correctamente'
    },
    fecha: {
        id: 'fecha',
        defaultMessage: 'Fecha'
    },
    guardando: {
        id: 'guardando',
        defaultMessage: 'Guardando...'
    },
    guardar: {
        id: 'guardar',
        defaultMessage: 'Guardar'
    },
    horario: {
        id: 'horario',
        defaultMessage: 'Horario'
    },
    horarioActualizado: {
        id: 'horario.actualizado',
        defaultMessage: 'Horario actualizado'
    },
    hora: {
        id: 'hora',
        defaultMessage: 'Hora'
    },
    horaInicio: {
        id: 'hora.inicio',
        defaultMessage: 'Hora de inicio'
    },
    horaInicioMayorFin: {
        id: 'hora.inicio.mayor.fin',
        defaultMessage: 'La hora de inicio es mayor que la de fin'
    },
    horaInicioObligatorio: {
        id: 'hora.inicio.obligatorio',
        defaultMessage: 'Introduzca una hora de inicio'
    },
    horaFin: {
        id: 'hora.fin',
        defaultMessage: 'Hora de fin'
    },
    horaFinObligatorio: {
        id: 'hora.fin.obligatorio',
        defaultMessage: 'Introduzca una hora de fin'
    },
    hoy: {
        id: 'hoy',
        defaultMessage: 'Hoy'
    },
    inicio: {
        id: 'inicio',
        defaultMessage: 'Inicio',
    },
    informacionActualizada: {
        id: 'informacion.actualizada',
        defaultMessage: 'Información actualizada correctamente'
    },
    invalidEmail: {
        id: 'invalid.email',
        defaultMessage: 'La dirección de correo dada no existe'
    },
    invalidToken: {
        id: 'invalid.token',
        defaultMessage: 'El email de reseteo de contraseña es incorrecto. Pruebe a enviar uno de nuevo'
    },
    limpiar: {
        id: 'limpiar',
        defaultMessage: 'Limpiar'
    },
    longitudMaxima: {
        id: 'longitud.maxima',
        defaultMessage: 'Longitud máxima {limit}'
    },
    medioComunicacion: {
        id: 'medio.comunicacion',
        defaultMessage: 'Medio comunicación'
    },
    mes: {
        id: 'mes',
        defaultMessage: 'Mes'
    },
    movil: {
        id: 'movil',
        defaultMessage: 'Móvil'
    },
    no: {
        id: 'no',
        defaultMessage: 'No'
    },
    nombre: {
        id: 'nombre',
        defaultMessage: 'Nombre'
    },
    nombreObligatorio: {
        id: 'nombre.obligatorio',
        defaultMessage: 'Introduzca un nombre'
    },
    nombrePeluqueria: {
        id: 'nombre.peluqueria',
        defaultMessage: 'Nombre de la peluquería'
    },
    nombrePeluqueriaObligatorio: {
        id: 'nombre.peluqueria.obligatorio',
        defaultMessage: 'Introduzca el nombre de la peluquería'
    },
    noResultsText: {
        id: 'no.results.text',
        defaultMessage: 'No se encontraron resultados'
    },
    nuevaFactura: {
        id: 'nueva.factura',
        defaultMessage: 'Nueva factura'
    },
    nuevoCliente: {
        id: 'nuevo.cliente',
        defaultMessage: 'Nuevo cliente'
    },
    nuevoProducto: {
        id: 'nuevo.producto',
        defaultMessage: 'Nuevo producto'
    },
    nuevoServicio: {
        id: 'nuevo.servicio',
        defaultMessage: 'Nuevo servicio'
    },
    observaciones: {
        id: 'observaciones',
        defaultMessage: 'Observaciones'
    },
    olvidastePassword: {
        id: 'olvidaste.password',
        defaultMessage: '¿Olvidaste la contraseña?'
    },
    password: {
        id: 'password',
        defaultMessage: 'Contraseña'
    },
    passwordActual: {
        id: 'password.actual',
        defaultMessage: 'Contraseña actual'
    },
    passwordActualObligatorio: {
        id: 'password.actual.obligatorio',
        defaultMessage: 'Introduzca la contraseña actual'
    },
    passwordIncorrecta: {
        id: 'password.incorrecta',
        defaultMessage: 'Contraseña incorrecta'
    },
    passwordLongitudMinima: {
        id: 'password.longitud.minima',
        defaultMessage: 'La contraseña debe tener al menos {limit} caracteres'
    },
    passwordNueva: {
        id: 'password.nueva',
        defaultMessage: 'Nueva contraseña'
    },
    passwordNuevaObligatorio: {
        id: 'password.nueva.obligatorio',
        defaultMessage: 'Introduzca la nueva contraseña'
    },
    passwordRepita: {
        id: 'password.repita',
        defaultMessage: 'Nueva contraseña (otra vez)'
    },
    passwordRepitaDiferente: {
        id: 'password.repita.diferente',
        defaultMessage: 'Las contraseñas no coinciden'
    },
    passwordRepitaObligatoria: {
        id: 'password.repita.obligatoria',
        defaultMessage: 'Introduzca la nueva contraseña otra vez'
    },
    passwordRestablecer: {
        id: 'password.restablecer',
        defaultMessage: 'Restablecer password'
    },
    passwordRestablecerMensaje: {
        id: 'password.restablecer.mensaje',
        defaultMessage: 'Introduzca su dirección de correo y le enviaremos un correo para restablecer la contraseña'
    },
    passwordRestablecerNuevaPassword: {
        id: 'password.restablecer.nueva.password',
        defaultMessage: 'Introduzca la nueva contraseña'
    },
    passwordRestablecida: {
        id: 'password.restablecida',
        defaultMessage: 'La contraseña se ha restablecido correctamente'
    },
    perfil: {
        id: 'perfil',
        defaultMessage: 'Perfil'
    },
    perfilActualizado: {
        id: 'perfil.actualizado',
        defaultMessage: 'Perfil actualizado correctamente'
    },
    precio: {
        id: 'precio',
        defaultMessage: 'Precio'
    },
    precioObligatorio: {
        id: 'precio.obligatorio',
        defaultMessage: 'Introduzca un precio'
    },
    precioPositivo: {
        id: 'precio.positivo',
        defaultMessage: 'El precio debe ser mayor que cero'
    },
    precioUnidad: {
        id: 'precio.unidad',
        defaultMessage: 'Precio unidad'
    },
    producto: {
        id: 'producto',
        defaultMessage: '{num, plural, one {producto} other {productos}}'
    },
    productoActualizado: {
        id: 'producto.actualizado',
        defaultMessage: 'Producto actualizado correctamente',
    },
    productoBorrado: {
        id: 'producto.borrado',
        defaultMessage: 'Productos borrados correctamente'
    },
    productoBorrarValor: {
        id: 'producto.borrar.valor',
        defaultMessage: 'Borrar producto'
    },
    productoCreado: {
        id: 'producto.creado',
        defaultMessage: 'Producto creado correctamente'
    },
    productoNombre: {
        id: 'producto.nombre',
        defaultMessage: 'Producto'
    },
    productos: {
        id: 'productos',
        defaultMessage: 'Productos'
    },
    registrarse: {
        id: 'registrarse',
        defaultMessage: 'Registrarse'
    },
    reset: {
        id: 'reset',
        defaultMessage: 'Resetear'
    },    
    semana: {
        id: 'semana',
        defaultMessage: 'Semana'
    },
    searchPromptText: {
        id: 'search.prompt.text',
        defaultMessage: 'Escriba para buscar'
    },
    seleccioneCliente: {
        id: 'seleccione.cliente',
        defaultMessage: 'Seleccione cliente'
    },
    seleccioneProducto: {
        id: 'seleccione.producto',
        defaultMessage: 'Seleccione producto'
    },
    seleccioneServicio: {
        id: 'seleccione.servicio',
        defaultMessage: 'Seleccione servicio'
    },
    servicio: {
        id: 'servicio',
        defaultMessage: 'Servicio'
    },
    servicios: {
        id: 'servicios',
        defaultMessage: 'Servicios'
    },
    servicioActualizado: {
        id: 'servicio.actualizado',
        defaultMessage: 'Servicio actualizado correctamente'
    },
    servicioBorrado: {
        id: 'servicio.borrado',
        defaultMessage: 'Servicios borrados correctamente'
    },
    servicioBorrarValor: {
        id: 'servicio.borrar.valor',
        defaultMessage: 'Borrar servicio'
    },
    servicioCreado: {
        id: 'servicio.creado',
        defaultMessage: 'Servicio creado correctamente'
    },
    servicioProductoObligatorio: {
        id: 'servicio.producto.obligatorio',
        defaultMessage: 'Debe al menos introducir un servicio o un producto'
    },
    si: {
        id: 'si',
        defaultMessage: 'Sí'
    },
    siguiente: {
        id: 'siguiente',
        defaultMessage: 'Siguiente'
    },
    sinFacturas: {
        id: 'sin.facturas',
        defaultMessage: 'Sin facturas para la fecha indicada'
    },
    sinResultados: {
        id: 'sin.resultados',
        defaultMessage: 'Sin resultados'
    },
    stock: {
        id: 'stock',
        defaultMessage: 'Stock'
    },
    stockEnteroPositivo: {
        id: 'stock.entero.positivo',
        defaultMessage: 'El stock debe ser un número entero mayor que cero'
    },
    tienesCuenta: {
        id: 'tienes.cuenta',
        defaultMessage: "¿Ya tienes cuenta?"
    },
    todoElDia: {
        id: 'todo.el.dia',
        defaultMessage: 'Todo el día'
    },
    total: {
        id: 'total',
        defaultMessage: 'Total'
    },
    trabajo: {
        id: 'trabajo',
        defaultMessage: 'Trabajo'
    },
    unprocessableEntity: {
        id: 'unprocessable.entity',
        defaultMessage: 'Entrada de datos incorrecta'
    }
});

export default messages;