import React from 'react';
import { render } from 'react-dom';
import configureStore from './configureStore';
import Root from './components/Root';
import { IntlProvider } from 'react-intl';
import { getLanguage, getLanguageWithoutRegionCode, getMessages } from './i18n';

// i18n
const language = getLanguage();
const languageWithoutRegionCode = getLanguageWithoutRegionCode(language);
const messages = getMessages(language, languageWithoutRegionCode);
// redux
const store = configureStore();

render(
    <IntlProvider locale={languageWithoutRegionCode} messages={messages}>
        <Root store={store} />
    </IntlProvider>,
    document.getElementById('app')
);
