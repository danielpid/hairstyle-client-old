import React, { Component } from 'react';
import Panel from 'react-bootstrap/lib/Panel';
import ListGroup from 'react-bootstrap/lib/ListGroup';
import ListGroupItem from 'react-bootstrap/lib/ListGroupItem';
import messages from './../../../i18n/messages';
import { formatMessage } from './../../../i18n';
import FacturaTable from './FacturaTable';
import LineaFacturaTable from './LineaFacturaTable';
import './FacturaPanel.css';

class FacturaPanel extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }

    toggleDetail() {
        this.setState({ open: !this.state.open });
    }

    render() {
        const { factura } = this.props;
        const stylePanel = {
            borderRadius: 0,
            marginBottom: 0
        }
        return (
            <div>
                <FacturaTable
                    factura={factura}
                    onClick={() => this.toggleDetail()}
                />
                <Panel collapsible expanded={this.state.open} className="FacturaPanel">
                    <ListGroup fill>
                        <ListGroupItem className="FacturaPanel-list-group-item">
                            <LineaFacturaTable factura={factura} />
                        </ListGroupItem>
                    </ListGroup>
                </Panel>
            </div>
        );
    }
}

export default FacturaPanel;