import { combineReducers } from 'redux';
import productos from './productos';
import productosDelete from './productosDelete';
import showModalAddProducto from './showModalAddProducto';
import showModalEditProducto from './showModalEditProducto';

const productosReducer = combineReducers({
    productos,
    productosDelete,
    showModalAddProducto,
    showModalEditProducto
});

export default productosReducer;