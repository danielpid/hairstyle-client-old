import React, { Component } from 'react';
import { connect } from 'react-redux';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import DropdownButton from 'react-bootstrap/lib/DropdownButton';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import isEmpty from 'lodash/isempty';
import * as connector from '../../reducers/peluqueria/peluqueria';
import * as actions from '../../actions/peluqueria';
import * as loginActions from '../../actions/login';
import messages from '../../i18n/messages';
import { formatMessage } from '../../i18n';
import './ProfileLink.css';

class ProfileLink extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        const { fetchPeluqueria } = this.props;
        fetchPeluqueria();
    }

    onSelectMenuItem(eventKey, event) {
        const { peluqueria, openModalProfile, logout } = this.props;
        switch (eventKey) {
            case '1':
                openModalProfile(peluqueria);
                break;
            case '2':
                logout();
                break;
        }
    }

    render() {
        const { peluqueria } = this.props;
        if (isEmpty(peluqueria)) {
            return <div />;
        }
        return (
            <div className="pull-right">
                <ButtonToolbar id="profile-button-toolbar">
                    <Image src=".../../favicon.png" circle className="ProfileLink-img" />
                    <DropdownButton
                        id="profile-split-button"
                        pullRight
                        bsStyle="link"
                        title={peluqueria.nombre}
                        onSelect={(eventKey, event) => this.onSelectMenuItem(eventKey, event)}
                    >
                        <MenuItem eventKey="1">{formatMessage(messages.perfil)}</MenuItem>
                        <MenuItem divider />
                        <MenuItem eventKey="2">{formatMessage(messages.cerrarSesion)}</MenuItem>
                    </DropdownButton>
                </ButtonToolbar>
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
    peluqueria: connector.getDataPeluqueria(state)
});

ProfileLink = connect(
    mapStateToProps,
    Object.assign(actions, loginActions)
)(ProfileLink);
export default ProfileLink;