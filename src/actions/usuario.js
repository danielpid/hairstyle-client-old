import * as api from '../api/usuario';
import { getIsResettingPassword } from '../reducers/usuario';
import { handleError } from './common';
import alert from './alert';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';

export const resetUserPassword = (token, password) => (dispatch, getState) => {
    if (getIsResettingPassword(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'RESET_PASSWORD_REQUEST'
    });
    return api.resetUserPassword(token, password).then(
        response => {
            dispatch({
                type: 'RESET_PASSWORD_SUCCESS'
            });
            alert('success', formatMessage(messages.passwordRestablecida));
        },
        error => {
            if (error.message == 401) {
                alert('error', formatMessage(messages.invalidToken));
            } else {
                handleError(dispatch, error, 'RESET_PASSWORD_FAILURE');
            }
        }
    );
}