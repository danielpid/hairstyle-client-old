import * as validations from './../../Validation/Validations';
import { formatMessage } from './../../../i18n';
import messages from './../../../i18n/messages';
import { maxlengthNombre } from '../../Clients/ClienteValidate';

const validate = (values) => {
    const errors = {};
    // día
    if (validations.isEmpty(values.fecha)) {
        errors.fecha = formatMessage(messages.diaObligatorio);
    }
    // inicio
    if (validations.isEmpty(values.inicio)) {
        errors.inicio = formatMessage(messages.horaInicioObligatorio);
    }
    // fin
    if (validations.isEmpty(values.fin)) {
        errors.fin = formatMessage(messages.horaFinObligatorio);
    }
    // inicio > fin
    if (!validations.isEmpty(values.inicio) 
        && !validations.isEmpty(values.fin)
        && values.inicio.isAfter(values.fin)) {
        errors.fin = formatMessage(messages.horaInicioMayorFin);
    }
    // cliente
    if (validations.isEmpty(values.cliente)) {
        errors.cliente = formatMessage(messages.clienteObligatorio);
    }
    return errors;
};

export default validate;