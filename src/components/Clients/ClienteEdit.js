import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { injectIntl } from 'react-intl';
import trim from 'lodash/trim';
import * as actions from '../../actions/clientes';
import * as connectors from '../../reducers/clientes/showModalEditCliente';
import messages from './../../i18n/messages';
import validate from './ClienteValidate';
import { formatMessage } from './../../i18n';
import ClienteModal from './ClienteModal';

class ClienteEdit extends Component {

    constructor(props) {
        super(props);
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
    }

    validateAndCreatePost(values) {
        const {editCliente, initialValues} = this.props;
        editCliente(initialValues.idCliente, trim(values.nombre), trim(values.observaciones), 
            initialValues.idMedioComunicacionMovil, trim(values.descripcionMedioComunicacionMovil), 
            initialValues.idMedioComunicacionCasa, trim(values.descripcionMedioComunicacionCasa), 
            initialValues.idMedioComunicacionTrabajo, trim(values.descripcionMedioComunicacionTrabajo), 
            initialValues.idMedioComunicacionEmail, trim(values.descripcionMedioComunicacionEmail));
    }

    render() {
        const {showModalEditCliente, closeModalEditCliente} = this.props;
        return (
            <ClienteModal
                showModalProp={showModalEditCliente}
                closeModalAction={closeModalEditCliente}
                validateAndCreatePost={this.validateAndCreatePost}
                idModal="editClienteModal"
                titleModal={formatMessage(messages.editarCliente)}
                {...this.props}
                {...this.props.intl} />
        );
    }
}

const reduxFormConfig = {
    form: 'clienteEditForm',
    validate,
    enableReinitialize: true
};

ClienteEdit = reduxForm(
    reduxFormConfig
)(ClienteEdit);

const mapStateToProps = (state) => ({
    showModalEditCliente: connectors.getOpen(state),
    initialValues: connectors.getCliente(state)
});

ClienteEdit = connect(
    mapStateToProps,
    actions
)(ClienteEdit);

export default injectIntl(ClienteEdit);