import React from 'react';
import { Link } from 'react-router'
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';
import Wait from './Wait';
import ConfirmRemove from './ConfirmRemove';
import ProfileLink from './Profile/ProfileLink';
import ProfileModal from './Profile/ProfileModal';

const HairStyleNavTabs = ({tabName}) => (
    <div>
        <ProfileLink />
        <ul className="nav nav-tabs">
            <li className={tabName == 'home' ? 'active' : ''}>
                <Link to="/">{formatMessage(messages.inicio)}</Link>
            </li>
            <li className={tabName == 'clients' ? 'active' : ''}>
                <Link to="/clientes">{formatMessage(messages.clientes)}</Link>
            </li>
            <li className={tabName == 'products' ? 'active' : ''}>
                <Link to="/productos">{formatMessage(messages.productos)}</Link>
            </li>
            <li className={tabName == 'services' ? 'active' : ''}>
                <Link to="/servicios">{formatMessage(messages.servicios)}</Link>
            </li>
        </ul>
        <Wait />
        <ConfirmRemove />
        <ProfileModal />
    </div>
);

export default HairStyleNavTabs;