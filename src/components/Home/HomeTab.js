import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Dimensions from 'react-dimensions';
import HairStyleNavTabs from './../HairStyleNavTabs';
import HSCalendar from './Citas/HSCalendar';
import CitaModal from './Citas/CitaModal';
import ConfigCalendarModal from './Citas/ConfigCalendarModal';
import Caja from './Facturas/Caja';

class HomeTab extends Component {

    render() {
        const { containerHeight } = this.props;
        const cssStyles = {
            height: containerHeight,
            paddingTop: 25,
            paddingBottom: 25
        };
        return (
            <div>
                <HairStyleNavTabs tabName='home' />
                <Grid fluid>
                    <Row bsClass="row-fluid">
                        <Col md={8} style={cssStyles}>
                            <ConfigCalendarModal />
                            <HSCalendar />
                            <CitaModal />
                        </Col>
                        <Col md={4} style={cssStyles}>
                            <Caja />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default Dimensions()(HomeTab);