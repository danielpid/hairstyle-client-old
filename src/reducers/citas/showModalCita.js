const showModalCita = (state = false, action) => {
    switch (action.type) {
        case 'OPEN_MODAL_CITA':
        case 'SET_CITA':
            return {
                open: true,
                cita: action.cita
            };
        case 'CLOSE_MODAL_CITA':
        case 'ADD_CITA_SUCCESS':
        case 'EDIT_CITA_SUCCESS':
        case 'REMOVE_CITA_SUCCESS':
            return {
                open: false,
                cita: null
            };
        default:
            return state;
    }
};

export default showModalCita;

export const getOpen = (state) =>
    state.citas.showModalCita.open;

export const getCita = (state) =>
    state.citas.showModalCita.cita;