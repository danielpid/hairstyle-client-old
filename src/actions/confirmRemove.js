export const openModalConfirmRemove = (removeAction, elementsToRemove, elementsName) => ({
    type: "OPEN_MODAL_CONFIRM_REMOVE",
    removeAction,
    elementsToRemove,
    elementsName
});

export const closeModalConfirmRemove = () => ({
    type: "CLOSE_MODAL_CONFIRM_REMOVE"
});