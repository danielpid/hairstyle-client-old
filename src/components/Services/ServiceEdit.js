import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { injectIntl } from 'react-intl';
import * as actions from '../../actions/servicios';
import * as connectors from '../../reducers/services/showModalEditService';
import messages from './../../i18n/messages';
import validate from './ServiceValidate';
import { formatMessage } from './../../i18n';
import ServiceModal from './ServiceModal';

class ServiceEdit extends Component {

    constructor(props) {
        super(props);
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
    }

    validateAndCreatePost(values) {
        const {editService, initialValues} = this.props;
        editService(initialValues.idServicio, values.nombre.trim(), values.precio);
    }

    render() {
        const {showModalEditService, closeModalEditService} = this.props;
        return (
            <ServiceModal 
                showModalProp={showModalEditService} 
                closeModalAction={closeModalEditService} 
                validateAndCreatePost={this.validateAndCreatePost}
                idModal="editServicioModal"
                titleModal={formatMessage(messages.editarServicio)} 
                {...this.props} 
                {...this.props.intl} />
        );
    }
}

const reduxFormConfig = {
    form: 'serviceEditForm',
    validate,
    enableReinitialize : true
};

ServiceEdit = reduxForm(
    reduxFormConfig
)(ServiceEdit);

const mapStateToProps = (state) => ({
    showModalEditService: connectors.getOpen(state),
    initialValues: connectors.getServicio(state)
});

ServiceEdit = connect(
    mapStateToProps,
    actions
)(ServiceEdit);

export default injectIntl(ServiceEdit);