import React, { PropTypes, Component } from 'react';
import { Provider, connect } from 'react-redux';
import { Router, Route, hashHistory } from 'react-router';
import Dimensions from 'react-dimensions';
import 'babel-polyfill';
import Alert from 'react-s-alert';
import * as connector from '../reducers/login';
import * as actions from '../actions/login';
import * as actionsPeluqueria from '../actions/peluqueria';
import * as actionsEmail from '../actions/email';
import * as actionsUsuario from '../actions/usuario';
import NotLoggedInContainer from './NotLoggedInContainer';
import NotLoggedInContainerWithWait from './NotLoggedInContainerWithWait';
import Login from './Login';
import CreateAccount from './CreateAccount';
import ForgotPassword from './ForgotPassword';
import ResetPassword from './ResetPassword';
import HomeTab from './Home/HomeTab';
import ClientsTab from './Clients/ClientsTab';
import ProductsTab from './Products/ProductsTab';
import ServicesTab from './Services/ServicesTab';
import './Root.css';

class Root extends Component {

    render() {
        const { containerHeight, store, isAuthenticated, isInvalidCredentials, login, hideInvalidCredentials,
            error, isCreatingAccount, createAccount, showLogin, addPeluqueria, hasForgottenPassword, forgotPassword,
            sendEmailResetPassword, resetUserPassword } = this.props;

       if (isCreatingAccount === true) {
            return (
                <div>
                    <Provider store={store}>
                        <NotLoggedInContainerWithWait>
                            <CreateAccount
                                showLogin={showLogin}
                                addPeluqueria={addPeluqueria}
                            />
                        </NotLoggedInContainerWithWait>
                    </Provider>
                    <Alert stack={{ limit: 3 }} position='top-right' effect='stackslide' timeout={5000} />
                </div>
            );
        } else if (hasForgottenPassword === true) {
            return (
                <div>
                    <Provider store={store}>
                        <NotLoggedInContainerWithWait>
                            <ForgotPassword
                                showLogin={showLogin}
                                sendEmailResetPassword={sendEmailResetPassword}
                            />
                        </NotLoggedInContainerWithWait>
                    </Provider>
                    <Alert stack={{ limit: 3 }} position='top-right' effect='stackslide'/>
                </div>
            );
        } else if (window.location.hash.includes('resetpassword')) {
            return (
                <div>
                    <Provider store={store}>
                        <NotLoggedInContainerWithWait>
                            <ResetPassword
                                resetUserPassword={resetUserPassword}
                            />
                        </NotLoggedInContainerWithWait>
                    </Provider>
                    <Alert stack={{ limit: 3 }} position='top-right' effect='stackslide'/>
                </div>
            );
        } else if (isAuthenticated === true || localStorage.getItem('hs_tkn_session')) {
            return (
                <div className="Root" style={{ height: containerHeight }}>
                    <Provider store={store}>
                        <Router history={hashHistory}>
                            <Route path="/" component={HomeTab} />
                            <Route path="/clientes" component={ClientsTab} />
                            <Route path="/productos" component={ProductsTab} />
                            <Route path="/servicios" component={ServicesTab} />
                        </Router>
                    </Provider>
                    <Alert stack={{ limit: 3 }} position='top-right' effect='stackslide' timeout={5000} />
                </div>
            );
        }
        return (
            <NotLoggedInContainer>
                <Login
                    login={login}
                    invalidCredentials={isInvalidCredentials}
                    hideInvalidCredentials={hideInvalidCredentials}
                    error={error}
                    createAccount={createAccount}
                    forgotPassword={forgotPassword}
                />
            </NotLoggedInContainer>
        );
    }
};

Root.propTypes = {
    store: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    isAuthenticated: connector.getIsAuthenticated(state),
    isInvalidCredentials: connector.getIsInvalidCredentials(state),
    error: connector.getError(state),
    isCreatingAccount: connector.getIsCreatingAccount(state),
    hasForgottenPassword: connector.getHasForgottenPassword(state)
});

Root = connect(
    mapStateToProps,
    Object.assign({}, actions, actionsPeluqueria, actionsEmail, actionsUsuario)
)(Root);

export default Dimensions({
    getHeight: () => (window.innerHeight - 50)
})(Root);