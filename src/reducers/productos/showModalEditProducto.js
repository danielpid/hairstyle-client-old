const showModalEditProducto = (state = {}, action) => {
    switch (action.type) {
        case 'OPEN_MODAL_EDIT_PRODUCTO':
            return {
                open: true,
                producto: action.producto
            };
        case 'CLOSE_MODAL_EDIT_PRODUCTO':
        case 'EDIT_PRODUCTO_SUCCESS':
            return {
                open: false,
                producto: null
            };
        default:
            return state;
    }
}

export default showModalEditProducto;

export const getOpen = (state) =>
    state.productos.showModalEditProducto.open;

export const getProducto = (state) =>
    state.productos.showModalEditProducto.producto;