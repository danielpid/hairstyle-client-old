import React from 'react';
import { Cell } from 'fixed-data-table';

const CheckboxCell = ({ rowIndex, dataListWrapper, col, onClickCheckbox, arrayDelete }) => {
    let input;
    const id = dataListWrapper.getObjectAt(rowIndex)[col];
    const cursorStyle = {
        cursor: 'pointer'
    }
    return (
        <Cell onClick={() => onClickCheckbox(input)} style={cursorStyle}>
            <input
                ref={node => { input = node; } }
                type="checkbox"
                value={id}
                checked={arrayDelete && arrayDelete.includes(id)}
                style={cursorStyle}
                />
        </Cell>
    );
};

export default CheckboxCell;