import fetch from 'isomorphic-fetch';
import * as api from './common';

const url = api.urlBase + '/api/servicios';

export const fetchServices = () => 
    api.fetchAll(url);

export const fetchServicesByCriteria = (nombre, precio) => 
    api.fetchByCriteria(url, {nombre, precio});

export const addService = (nombre, precio) => 
    api.add(url, {nombre, precio});

export const editService = (idServicio, nombre, precio) => 
    api.edit(url, {idServicio, nombre, precio});

export const removeServices = (idsServicio) => 
    api.remove(url, idsServicio);