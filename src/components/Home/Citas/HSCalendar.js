import React, { Component } from 'react';
import { connect } from 'react-redux';
import BigCalendar from 'react-big-calendar';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd'
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop';
import moment from 'moment';
import isEmpty from 'lodash/isempty';
import * as actions from '../../../actions/citas';
import * as connectorCitas from '../../../reducers/citas/citas';
import * as connectorPeluqueria from '../../../reducers/peluqueria/peluqueria';
import { locale } from './../../../i18n';
import { views, messagesCalendar, formats } from './HSCalendarConfig';

// Setup the localizer by providing the moment (or globalize) Object to the correct localizer.
BigCalendar.momentLocalizer(moment);
const DragAndDropCalendar = withDragAndDrop(BigCalendar);

class HSCalendar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            view: 'week',
            referenceDate: moment()
        };
    }

    componentDidMount() {
        this.getEvents('week');
    }

    onView(value) {
        this.state.view = value;
        this.getEvents(value);
    }

    onNavigate(date, view) {
        this.state.referenceDate = moment(date);
        this.getEvents(view);
    }

    onSelectSlot(slotInfo) {
        const { openModalCita } = this.props;
        openModalCita({
            fecha: moment(slotInfo.start),
            inicio: moment(slotInfo.start),
            fin: moment(slotInfo.end),
            cliente: null
        });
    }

    onSelectEvent(event) {
        const { openModalCita } = this.props;
        openModalCita({
            idCita: event.idCita,
            fecha: moment(event.fechaDesde),
            inicio: moment(event.fechaDesde),
            fin: moment(event.fechaHasta),
            cliente: {
                idCliente: event.idCliente,
                nombre: event.nombreCliente
            }
        });
    }

    onEventDrop({ event, start, end }) {
        const { editCita } = this.props;
        const newCita = {
            idCita: event.idCita,
            idCliente: event.idCliente,
            nombreCliente: event.nombreCliente,
            fechaDesde: moment(start),
            fechaHasta: moment(start).add(moment(event.fechaHasta).diff(moment(event.fechaDesde)))
        };
        editCita(newCita.idCita, newCita.idCliente, newCita.nombreCliente, newCita.fechaDesde, newCita.fechaHasta);
    }

    getEvents(view) {
        const { fetchCitasByCriteria } = this.props;
        view = view === 'agenda' ? 'week' : view;
        const fechaDesde = moment(this.state.referenceDate).startOf(view);
        const fechaHasta = moment(this.state.referenceDate).endOf(view);
        // el calendario puede mostrar fechas de otros meses
        if (view === 'month') {
            fechaDesde.subtract('day', 7);
            fechaHasta.add('day', 7);
        } else if (view === 'day') {
            fechaDesde.subtract('day', 1);
            fechaHasta.add('day', 1);
        }
        fetchCitasByCriteria(fechaDesde, fechaHasta);
    }

    render() {
        const { citas, peluqueria } = this.props;
        const minHour = peluqueria.horaApertura ?
            moment(peluqueria.horaApertura, 'HH:mm').toDate() : moment().hours(8).minutes(0).seconds(0).toDate();
        const maxHour = peluqueria.horaCierre ?
            moment(peluqueria.horaCierre, 'HH:mm').toDate() : moment().hours(22).minutes(0).seconds(0).toDate();
        if (isEmpty(peluqueria)) {
            // evita descuadre calendar
            return (
                <div />
            );
        }
        return (
            <DragAndDropCalendar
                selectable='ignoreEvents'
                popup
                events={citas}
                titleAccessor='nombreCliente'
                startAccessor='fechaDesde'
                endAccessor='fechaHasta'
                views={views}
                defaultView={this.state.view}
                culture={locale}
                min={minHour}
                max={maxHour}
                step={30}
                timeslot={10}
                onView={value => this.onView(value)}
                onNavigate={(date, view) => this.onNavigate(date, view)}
                onSelectSlot={(slotInfo) => this.onSelectSlot(slotInfo)}
                onSelectEvent={event => this.onSelectEvent(event)}
                onEventDrop={(args) => this.onEventDrop(args)}
                messages={messagesCalendar}
                formats={formats}
                ref='hsCalendar'
            />
        );
    }
}

const mapStateToProps = (state) => ({
    citas: connectorCitas.getCitas(state),
    peluqueria: connectorPeluqueria.getDataPeluqueria(state)
});

HSCalendar = connect(
    mapStateToProps,
    actions
)(HSCalendar);

export default DragDropContext(HTML5Backend)(HSCalendar);
