import React, { Component } from 'react';
import { connect } from 'react-redux';
import Table from 'react-bootstrap/lib/Table';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import messages from './../../../i18n/messages';
import { formatMessage, formatTime } from './../../../i18n';
import * as confirmRemove from '../../../actions/confirmRemove';
import * as actions from '../../../actions/facturas';
import './FacturaTable.css';

class FacturaTable extends Component {

    showEditFacturaModal(factura) {
        const {openModalFactura} = this.props;
        openModalFactura(factura);
    }

    showConfirmDeleteModal(factura) {
        const { openModalConfirmRemove, removeFactura } = this.props;
        openModalConfirmRemove(removeFactura, factura.idFactura,
            formatMessage(messages.eliminarFactura, { nombreCliente: factura.nombreCliente }));
    }

    render() {
        const { onClick, factura } = this.props;
        return (
            <Table striped bordered className="FacturaTable">
                <tbody>
                    <tr className="FacturaTable-tr">
                        <td onClick={onClick} className="FacturaTable-td-nombre">
                            {factura.nombreCliente}
                        </td>
                        <td onClick={onClick} className="FacturaTable-td-fecha" title={formatMessage(messages.hora)}>
                            {formatTime(factura.fechaAlta, {})}
                        </td>
                        <td onClick={onClick} className="FacturaTable-td-precio">
                            {factura.precio} €
                        </td>
                        <td>
                            <Glyphicon
                                glyph="edit"
                                onClick={() => this.showEditFacturaModal(factura)}
                            />
                        </td>
                        <td>
                            <Glyphicon
                                glyph="trash"
                                onClick={() => this.showConfirmDeleteModal(factura)}
                            />
                        </td>
                    </tr>
                </tbody>
            </Table>
        );
    }
}

const mapStateToProps = (state) => ({});

export default FacturaTable = connect(
    mapStateToProps,
    Object.assign(actions, confirmRemove)
)(FacturaTable);