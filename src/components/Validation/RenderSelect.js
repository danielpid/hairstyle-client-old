import React, { Component } from 'react';
import Select from 'react-select';
import messages from './../../i18n/messages';
import { formatMessage } from './../../i18n';

class RenderSelect extends Component {

    onChange(value) {
        value = value || '';
        const { name, additionalActions } = this.props;
        if (additionalActions) {
            additionalActions(name, value);
        } 
    }

    render() {
        const { input, label, placeholder, defaultValue, meta: { touched, error, invalid, warning },
            loadOptions, valueKey, labelKey, clearValueText } = this.props;
        const labelHtml = label ? <label className="control-label">{label}</label> : '';
        return (
            <div className={`form-group ${touched && error ? 'has-error' : ''}`}>
                {labelHtml}
                <div>
                    <Select.Async
                        value={this.props.input.value}
                        onChange={(value) => this.onChange(value)}
                        loadOptions={loadOptions}
                        valueKey={valueKey}
                        labelKey={labelKey}
                        clearValueText={clearValueText}
                        noResultsText={formatMessage(messages.noResultsText)}
                        placeholder={placeholder}
                        searchPromptText={formatMessage(messages.searchPromptText)}
                        loadingPlaceholder={formatMessage(messages.buscando)}
                        ignoreCase={false}
                        filterOption={() => true}
                    />
                    <div className="help-block">
                        {touched && error && <span>{error}</span>}
                    </div>
                </div>
            </div>
        );
    }
}

export default RenderSelect;