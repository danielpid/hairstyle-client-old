const showModalAddProducto = (state = false, action) => {
    switch (action.type) {
        case 'OPEN_MODAL_ADD_PRODUCTO':
            return true;
        case 'CLOSE_MODAL_ADD_PRODUCTO':
            return false;
        default:
            return state;
    }
}

export default showModalAddProducto;