import fetch from 'isomorphic-fetch';
import { urlBase } from './common';

const url = urlBase + '/login';

export const login = (email, password) => (
    fetch(url, {
        method: 'POST',
        body: JSON.stringify({ username: email, password: password })
    }).then(response => {
        if (response.status >= 400) {
            throw new Error(response.status);
        }
        return response.headers.get('Authorization');
    })
);