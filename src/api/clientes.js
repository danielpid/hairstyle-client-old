import fetch from 'isomorphic-fetch';
import * as api from './common';

const url = api.urlBase + '/api/clientes';

export const fetchClientes = () =>
    api.fetchAll(url);

export const fetchClientesByCriteria = (nombre, observaciones, descripcionMedioComunicacion) =>
    api.fetchByCriteria(url, { nombre, observaciones, descripcionMedioComunicacion });

export const addCliente = (nombre, observaciones, descripcionMedioComunicacionMovil, descripcionMedioComunicacionCasa, 
    descripcionMedioComunicacionTrabajo, descripcionMedioComunicacionEmail) =>
    api.add(url, { nombre, observaciones, descripcionMedioComunicacionMovil, descripcionMedioComunicacionCasa, 
        descripcionMedioComunicacionTrabajo, descripcionMedioComunicacionEmail });

export const editCliente = (idCliente, nombre, observaciones, idMedioComunicacionMovil, 
    descripcionMedioComunicacionMovil, idMedioComunicacionCasa, descripcionMedioComunicacionCasa, 
    idMedioComunicacionTrabajo, descripcionMedioComunicacionTrabajo, idMedioComunicacionEmail, 
    descripcionMedioComunicacionEmail) =>
    api.edit(url, { idCliente, nombre, observaciones, idMedioComunicacionMovil, descripcionMedioComunicacionMovil, 
        idMedioComunicacionCasa, descripcionMedioComunicacionCasa, idMedioComunicacionTrabajo,
        descripcionMedioComunicacionTrabajo, idMedioComunicacionEmail, descripcionMedioComunicacionEmail });

export const removeClientes = (idsCliente) =>
    api.remove(url, idsCliente);