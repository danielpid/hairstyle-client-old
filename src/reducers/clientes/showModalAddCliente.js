const showModalAddCliente = (state = false, action) => {
    switch (action.type) {
        case 'OPEN_MODAL_ADD_CLIENTE':
            return true;
        case 'CLOSE_MODAL_ADD_CLIENTE':
            return false;
        default:
            return state;
    }
}

export default showModalAddCliente;