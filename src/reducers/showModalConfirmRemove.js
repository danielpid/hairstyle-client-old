const showModalConfirmRemove = (state = false, action) => {
    switch (action.type) {
        case 'OPEN_MODAL_CONFIRM_REMOVE':
            return {
                showing: true,
                removeAction: action.removeAction,
                elementsToRemove: action.elementsToRemove,
                elementsName: action.elementsName
            };
        case 'CLOSE_MODAL_CONFIRM_REMOVE':
        case 'REMOVE_CLIENTES_SUCCESS':
        case 'REMOVE_PRODUCTOS_SUCCESS':
        case 'REMOVE_SERVICES_SUCCESS':
        case 'REMOVE_FACTURA_SUCCESS':
            return {
                showing: false
            };
        default:
            return state;
    }
};

export default showModalConfirmRemove;