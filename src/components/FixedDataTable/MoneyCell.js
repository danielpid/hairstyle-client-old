import React from 'react';
import { Cell } from 'fixed-data-table';

const MoneyCell = ({ rowIndex, data, col, moneySymbol, ...props }) => (
    <Cell {...props}>
        {data.getObjectAt(rowIndex)[col]} {moneySymbol}
    </Cell>
);

export default MoneyCell;