import alert from './alert';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';
import { format } from 'path';
import { logout } from './login';

export const handleError = (dispatch, error, action) => {
    if (error.message == 403) {
        logout()(dispatch);
    } else if (error.message == 422) {
        alert('error', formatMessage(messages.unprocessableEntity));
    } else {
        alert('error', formatMessage(messages.errorGenerico));
    }
    dispatch({
        type: action
    });
};