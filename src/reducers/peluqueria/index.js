import { combineReducers } from 'redux';
import showModalProfile from './showModalProfile';
import peluqueria from './peluqueria';

const peluqueriaReducers = combineReducers({
    showModalProfile,
    peluqueria
});

export default peluqueriaReducers;