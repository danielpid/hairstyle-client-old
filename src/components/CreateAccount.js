import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Button from 'react-bootstrap/lib/Button';
import renderField from './Validation/RenderField';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';
import moment from 'moment';
import validate, { asyncValidate } from './CreateAccountValidate';
import './CreateAccount.css';

class CreateAccount extends Component {

    constructor(props) {
        super(props);
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
    }

    validateAndCreatePost(values) {
        const { addPeluqueria } = this.props;
        addPeluqueria(values.nombre, values.email, moment().hour(9).minute(0).second(0).toDate(),
            moment().hour(21).minute(0).second(0).toDate(), values.passwordNueva, values.passwordNueva);
    }

    render() {
        const { showLogin, handleSubmit } = this.props;
        return (
            <form onSubmit={handleSubmit(this.validateAndCreatePost)}>
                <FormGroup>
                    <Field
                        name="nombre" type="text" component={renderField}
                        placeholder={formatMessage(messages.nombrePeluqueria)}
                    />
                    <Field
                        name="email" type="text" component={renderField}
                        placeholder={formatMessage(messages.email)}
                    />
                    <Field
                        name="passwordNueva" type="password" component={renderField}
                        placeholder={formatMessage(messages.password)}
                    />
                    <Field
                        name="passwordRepita" type="password" component={renderField}
                        placeholder={formatMessage(messages.passwordRepita)}
                    />
                </FormGroup>
                <Button bsStyle="success" className="CreateAccount-button" type="submit">
                    {formatMessage(messages.registrarse)}
                </Button>
                <div className="CreateAccount-login">
                    {formatMessage(messages.tienesCuenta)}
                    <Button bsStyle="link" className="CreateAccount-button-link"
                        onClick={() => showLogin()}>
                        {formatMessage(messages.entrar)}
                    </Button>
                </div>
            </form >
        );
    }
}

const reduxFormConfig = {
    form: 'registerForm',
    validate,
    asyncValidate,
    asyncBlurFields: ['email']
};

CreateAccount = reduxForm(
    reduxFormConfig
)(CreateAccount);

export default CreateAccount;