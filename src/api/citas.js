import fetch from 'isomorphic-fetch';
import * as api from './common';

const url = api.urlBase + '/api/citas';

export const fetchCitasByCriteria = (fechaDesde, fechaHasta) =>
    api.fetchByCriteria(url, { fechaDesde, fechaHasta });

export const addCita = (idCliente, nombreCliente, fechaDesde, fechaHasta) =>
    api.add(url, { idCliente, nombreCliente, fechaDesde, fechaHasta });

export const editCita = (idCita, idCliente, nombreCliente, fechaDesde, fechaHasta) =>
    api.edit(url, {idCita, idCliente, nombreCliente, fechaDesde, fechaHasta});

export const removeCita = (idCita) => 
    api.removeById(url, idCita);