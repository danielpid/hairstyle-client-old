import fetch from 'isomorphic-fetch';
import * as api from './common';

const url = api.urlBase + '/api/peluqueria';

export const fetchPeluqueria = () => (
    fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('hs_tkn_session')
        }
    }).then((response) => {
        if (response.status >= 400) {
            throw new Error(response.status);
        }
        return response.json();
    })
);

export const addPeluqueria = (nombre, email, horaApertura, horaCierre, passwordActual, passwordNueva) =>
    api.add(url, { nombre, email, horaApertura, horaCierre, passwordActual, passwordNueva });

export const editPeluqueria = (nombre, email, horaApertura, horaCierre, passwordActual, passwordNueva) =>
    api.edit(url, { nombre, email, horaApertura, horaCierre, passwordActual, passwordNueva });
