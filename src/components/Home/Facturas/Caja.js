import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import * as connector from '../../../reducers/facturas/facturas';
import * as actions from '../../../actions/facturas';
import messages from './../../../i18n/messages';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Alert from 'react-bootstrap/lib/Alert';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import DatePicker from 'react-datepicker';
import PageHeader from 'react-bootstrap/lib/PageHeader';
import FacturaPanel from './FacturaPanel';
import { getDateFormat, locale, formatMessage } from './../../../i18n';
import FacturaModal from './FacturaModal';
import './Caja.css';

class Caja extends Component {

    constructor(props) {
        super(props);
        this.state = {
            referenceDate: moment()
        };
    }

    componentDidMount() {
        this.getFacturasByFecha();
    }

    getFacturasByFecha() {
        const { fetchFacturasByCriteria } = this.props;
        const fechaDesde = moment(this.state.referenceDate).startOf('day');
        const fechaHasta = moment(this.state.referenceDate).endOf('day');
        fetchFacturasByCriteria(fechaDesde, fechaHasta);
    }

    handleOnChange(value) {
        this.state.referenceDate = value;
        this.getFacturasByFecha();
    }

    openModalFactura(factura) {
        const { openModalFactura } = this.props;
        openModalFactura({
            servicios: [{}],
            productos: [{}]
        });
    }

    render() {
        const { facturas } = this.props;
        const dateFormat = getDateFormat();

        let facturasList;
        if (facturas.length === 0) {
            facturasList = (
                <Alert className="Caja-alert">
                    {formatMessage(messages.sinFacturas)}
                </Alert>
            );
        } else {
            facturasList = (
                <div>
                    {facturas.map(factura =>
                        <FacturaPanel
                            key={factura.idFactura}
                            factura={factura}
                            openModalFactura={() => this.openModalFactura(factura)}
                        />
                    )}
                </div>
            );
        }
        return (
            <div id="caja-component">
                <Row bsClass="row-fluid">
                    <Col md={4} className="Caja-col-date-picker">
                        <DatePicker
                            todayButton={formatMessage(messages.hoy)}
                            dateFormat={dateFormat}
                            selected={this.state.referenceDate}
                            className="form-control"
                            locale={locale}
                            onChange={(oMoment) => this.handleOnChange(oMoment)}
                        />
                    </Col>
                    <Col md={4}>
                        <h3 className="Caja-title">{formatMessage(messages.caja)}</h3>
                    </Col>
                    <Col md={4} className="Caja-col-add">
                        <Button bsStyle="success" className="Caja-button pull-right" onClick={() => this.openModalFactura()}>
                            <Glyphicon className="Caja-glyphicon" glyph="plus-sign" /> {formatMessage(messages.nuevaFactura)}
                        </Button>
                    </Col>
                </Row>
                <Row bsClass="row-fluid">
                    <Col md={12} className="Caja-facturas-list">
                        {facturasList}
                    </Col>
                </Row>
                <FacturaModal />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    facturas: connector.getFacturas(state)
});

Caja = connect(
    mapStateToProps,
    actions
)(Caja);

export default Caja;