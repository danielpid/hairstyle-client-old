import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Button from 'react-bootstrap/lib/Button';
import renderField from './Validation/RenderField';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';
import validate from './ForgotPasswordValidate';
import './ForgotPassword.css';

class ForgotPassword extends Component {

    constructor(props) {
        super(props);
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
    }

    validateAndCreatePost(values) {
        const { sendEmailResetPassword } = this.props;
        sendEmailResetPassword(values.email);
    }

    render() {
        const { showLogin, handleSubmit } = this.props;
        return (
            <div>
                <p className="ForgotPassword-title">{formatMessage(messages.passwordRestablecer)}</p>
                <p className="ForgotPassword-message">{formatMessage(messages.passwordRestablecerMensaje)}</p>
                <form onSubmit={handleSubmit(this.validateAndCreatePost)}>
                    <FormGroup>
                        <Field
                            name="email" type="text" component={renderField}
                            placeholder={formatMessage(messages.email)}
                        />
                    </FormGroup>
                    <Button bsStyle="success" className="ForgotPassword-button" type="submit">
                        {formatMessage(messages.enviar)}
                    </Button>
                    <div className="ForgotPassword-login">
                        {formatMessage(messages.tienesCuenta)}
                        <Button bsStyle="link" className="ForgotPassword-button-link" onClick={() => showLogin()}>
                            {formatMessage(messages.entrar)}
                        </Button>
                    </div>
                </form >
            </div>
        );
    }
}

const reduxFormConfig = {
    form: 'forgotPasswordForm',
    validate
};

ForgotPassword = reduxForm(
    reduxFormConfig
)(ForgotPassword);

export default ForgotPassword;