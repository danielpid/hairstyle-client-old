import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/confirmRemove';
import Modal from 'react-bootstrap/lib/Modal';
import Alert from 'react-bootstrap/lib/Alert';
import Button from 'react-bootstrap/lib/Button';
import { formatMessage } from './../i18n';
import messages from './../i18n/messages';

let ConfirmRemove = ({ showModalConfirmRemove, closeModalConfirmRemove }) => (
    <Modal id="confirmRemoveModal" show={showModalConfirmRemove.showing} onHide={() => closeModalConfirmRemove()}>
        <Modal.Header closeButton>
            <Modal.Title>{formatMessage(messages.confirmarBorrado)}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Alert bsStyle="warning">
                {formatMessage(
                    messages.confirmarBorradoMensaje,
                    {
                        num: showModalConfirmRemove.elementsToRemove ?
                            showModalConfirmRemove.elementsToRemove.length : 0,
                        elementos: showModalConfirmRemove.elementsName
                    }
                )}
            </Alert>
        </Modal.Body>
        <Modal.Footer>
            <Button
                onClick={() => closeModalConfirmRemove()} >
                {formatMessage(messages.no)}
            </Button>
            <Button
                bsStyle="primary"
                onClick={() => showModalConfirmRemove.removeAction(showModalConfirmRemove.elementsToRemove)}>
                {formatMessage(messages.si)}
            </Button>
        </Modal.Footer>
    </Modal>
);

const mapStateToProps = (state) => ({
    showModalConfirmRemove: state.showModalConfirmRemove
});

ConfirmRemove = connect(
    mapStateToProps,
    actions
)(ConfirmRemove);

export default ConfirmRemove;

