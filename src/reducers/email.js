export const isSendingEmailResetPassword = (state = false, action) => {
    switch (action.type) {
        case "SEND_EMAIL_RESET_PASSWORD_REQUEST":
            return true;
        case "SEND_EMAIL_RESET_PASSWORD_SUCCESS":
        case "SEND_EMAIL_RESET_PASSWORD_FAILURE":
            return false;
        default:
            return state;
    }
}

export const getIsSendingEmailResetPassword = (state) =>
    state.isSendingEmailResetPassword;