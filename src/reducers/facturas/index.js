import { combineReducers } from 'redux';
import showModalFactura from './showModalFactura';
import facturas from './facturas';

const facturasReducers = combineReducers({
    showModalFactura,
    facturas
});

export default facturasReducers;