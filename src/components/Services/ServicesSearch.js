import React from 'react';
import { connect } from 'react-redux';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import messages from './../../i18n/messages';
import { formatMessage } from './../../i18n';
import { fetchServicesByCriteria } from '../../actions/servicios';
import './ServicesSearch.css';

let ServicesSearch = ({dispatch}) => {

    function searchIfEnter(e, inputNombre, inputPrecio) {
        if (e.keyCode === 13) {
            searchByCriteria(inputNombre, inputPrecio);
        }
    }

    function searchByCriteria(inputNombre, inputPrecio) {
        dispatch(fetchServicesByCriteria(inputNombre.value.trim(), inputPrecio ? inputPrecio.value : ""));
    }

    function clean(inputNombre, inputPrecio) {
        inputNombre.value = "";
        inputPrecio.value = "";
    }

    let inputNombre, inputPrecio;
    return (
        <Row bsClass="row-fluid" className="ServicesSearch">
            <Col md={4}>
                <input
                    ref={node => { inputNombre = node; } }
                    className="form-control"
                    type="text"
                    placeholder={formatMessage(messages.nombre)}
                    onKeyUp={e => searchIfEnter(e, inputNombre, inputPrecio)}
                    />
            </Col>
            <Col md={1}>
                <input
                    ref={node => { inputPrecio = node; } }
                    className="form-control"
                    type="number"
                    placeholder={formatMessage(messages.precio)}
                    onKeyUp={e => searchIfEnter(e, inputNombre, inputPrecio)}
                    />
            </Col>
            <Col md={1}>
                <Button bsStyle="primary" type="button" onClick={() => searchByCriteria(inputNombre, inputPrecio)} >
                    <Glyphicon glyph="search" /> {formatMessage(messages.buscar)}
                </Button>
            </Col>
            <Col md={1}>
                <Button type="button" onClick={() => clean(inputNombre, inputPrecio)} >
                    <Glyphicon glyph="erase" /> {formatMessage(messages.limpiar)}
                </Button>
            </Col>
        </Row>
    );
};

ServicesSearch = connect()(ServicesSearch);

export default ServicesSearch;