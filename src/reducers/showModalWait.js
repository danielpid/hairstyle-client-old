import { formatMessage } from './../i18n';
import messages from './../i18n/messages';

const showModalWait = (state = false, action) => {
    switch (action.type) {
        case "FETCH_SERVICES_REQUEST":
        case "FETCH_PRODUCTOS_REQUEST":
        case "FETCH_CLIENTES_REQUEST":
        case "FETCH_CITAS_REQUEST":
            return {
                showing: true,
                msg: formatMessage(messages.buscando)
            };
        case "ADD_SERVICE_REQUEST":
        case "ADD_PRODUCTO_REQUEST":
        case "ADD_CLIENTE_REQUEST":
        case "ADD_CITA_REQUEST":
        case "ADD_PELUQUERIA_REQUEST":
            return {
                showing: true,
                msg: formatMessage(messages.guardando)
            };
        case "EDIT_SERVICE_REQUEST":
        case "EDIT_PRODUCTO_REQUEST":
        case "EDIT_CLIENTE_REQUEST":
        case "EDIT_CITA_REQUEST":
            return {
                showing: true,
                msg: formatMessage(messages.actualizando)
            };
        case "REMOVE_SERVICES_REQUEST":
        case "REMOVE_PRODUCTOS_REQUEST":
        case "REMOVE_CLIENTES_REQUEST":
        case "REMOVE_CITA_REQUEST":
            return {
                showing: true,
                msg: formatMessage(messages.borrando)
            };
        case "SEND_EMAIL_RESET_PASSWORD_REQUEST":
            return {
                showing: true,
                msg: formatMessage(messages.enviando)
            }
        case 'FETCH_SERVICES_SUCCESS':
        case 'FETCH_SERVICES_FAILURE':
        case "ADD_SERVICE_SUCCESS":
        case "ADD_SERVICE_FAILURE":
        case 'EDIT_SERVICE_SUCCESS':
        case 'EDIT_SERVICE_FAILURE':
        case "REMOVE_SERVICES_SUCCESS":
        case "REMOVE_SERVICES_FAILURE":
        case "FETCH_PRODUCTOS_SUCCESS":
        case "FETCH_PRODUCTOS_FAILURE":
        case "ADD_PRODUCTO_SUCCESS":
        case "ADD_PRODUCTO_FAILURE":
        case "EDIT_PRODUCTO_SUCCESS":
        case "EDIT_PRODUCTO_FAILURE":
        case "REMOVE_PRODUCTOS_SUCCESS":
        case "REMOVE_PRODUCTOS_FAILURE":
        case "FETCH_CLIENTES_SUCCESS":
        case "FETCH_CLIENTES_FAILURE":
        case "ADD_CLIENTE_SUCCESS":
        case "ADD_CLIENTE_FAILURE":
        case "EDIT_CLIENTE_SUCCESS":
        case "EDIT_CLIENTE_FAILURE":
        case "REMOVE_CLIENTES_SUCCESS":
        case "REMOVE_CLIENTES_FAILURE":
        case "FETCH_CITAS_SUCCESS":
        case "FETCH_CITAS_FAILURE":
        case "ADD_CITA_SUCCESS":
        case "ADD_CITA_FAILURE":
        case "EDIT_CITA_SUCCESS":
        case "EDIT_CITA_FAILURE":
        case "REMOVE_CITA_SUCCESS":
        case "REMOVE_CITA_FAILURE":
        case "ADD_PELUQUERIA_SUCCESS":
        case "ADD_PELUQUERIA_FAILURE":
        case "SEND_EMAIL_RESET_PASSWORD_SUCCESS":
        case "SEND_EMAIL_RESET_PASSWORD_FAILURE":
            return {
                showing: false,
                msg: ''
            };
        default:
            return state;
    }
}

export default showModalWait;