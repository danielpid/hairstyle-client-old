import { combineReducers } from 'redux';
import showModalCita from './showModalCita';
import citas from './citas';

const citasReducers = combineReducers({
    showModalCita,
    citas
});

export default citasReducers;