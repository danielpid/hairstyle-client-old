import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import Modal from 'react-bootstrap/lib/Modal';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Button from 'react-bootstrap/lib/Button';
import Collapse from 'react-bootstrap/lib/Collapse';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import renderField from './../Validation/RenderField';
import messages from './../../i18n/messages';
import { formatMessage } from './../../i18n';
import * as connectors from '../../reducers/peluqueria/showModalProfile';
import * as actions from '../../actions/peluqueria';
import validate, { asyncValidate } from './ProfileValidate';
import './ProfileModal.css';

class ProfileModal extends Modal {

    constructor(props) {
        super(props);
        this.state = {};
        this.validateAndCreatePost = this.validateAndCreatePost.bind(this);
        this.closeModalProfileCallback = this.closeModalProfileCallback.bind(this);
    }

    validateAndCreatePost(values) {
        const { editPeluqueria, initialValues, closeModalProfile } = this.props;
        editPeluqueria(values.nombre, values.email, initialValues.horaApertura, initialValues.horaCierre,
            values.passwordActual, values.passwordNueva, messages.perfilActualizado, this.closeModalProfileCallback);
    }

    toggleChangePassword() {
        const open = !this.state.open;
        this.setState({ open: open });
        if (!open) {
            const { change } = this.props;
            change('passwordActual', null);
            change('passwordNueva', null);
            change('passwordRepita', null);
        }
    }

    closeModalProfileCallback() {
        const { closeModalProfile } = this.props;
        this.setState({ open: false });
        closeModalProfile();
    }

    render() {
        const { showModalProfile, handleSubmit, reset } = this.props;
        const symbol = this.state.open ? "-" : "+";
        return (
            <Modal id="profileEdit" show={showModalProfile} onHide={this.closeModalProfileCallback} onExited={reset}>
                <form onSubmit={handleSubmit(this.validateAndCreatePost)}>
                    <Modal.Header closeButton>
                        <Modal.Title>{formatMessage(messages.perfil)}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FormGroup>
                            <Field
                                name="nombre" type="text" component={renderField}
                                label={formatMessage(messages.nombre) + ' *'}
                                placeholder={formatMessage(messages.nombre)}
                            />
                            <Field
                                name="email" type="text" component={renderField}
                                label={formatMessage(messages.email) + ' *'}
                                placeholder={formatMessage(messages.email)}
                            />
                            <Button onClick={() => this.toggleChangePassword()}
                                bsStyle="link" className="ProfileModal-button-link">
                                {symbol} {formatMessage(messages.cambiarPassword)}
                            </Button>
                            <Collapse in={this.state.open}>
                                <div>
                                    <Field
                                        name="passwordActual" type="password" component={renderField}
                                        label={formatMessage(messages.passwordActual) + ' *'}
                                        placeholder={formatMessage(messages.passwordActual)}
                                    />
                                    <Field
                                        name="passwordNueva" type="password" component={renderField}
                                        label={formatMessage(messages.passwordNueva) + ' *'}
                                        placeholder={formatMessage(messages.passwordNueva)}
                                    />
                                    <Field
                                        name="passwordRepita" type="password" component={renderField}
                                        label={formatMessage(messages.passwordRepita) + ' *'}
                                        placeholder={formatMessage(messages.passwordRepita)}
                                    />
                                </div>
                            </Collapse>
                        </FormGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.closeModalProfileCallback} >
                            <Glyphicon glyph="remove" /> {formatMessage(messages.cerrar)}
                        </Button>
                        <Button bsStyle="primary" type="submit">
                            <Glyphicon glyph="floppy-disk" /> {formatMessage(messages.guardar)}
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        );
    }
}

const reduxFormConfig = {
    form: 'profileForm',
    validate,
    asyncValidate,
    asyncBlurFields: ['email'],
    enableReinitialize: true,
    keepDirtyOnReinitialize: true
};

ProfileModal = reduxForm(
    reduxFormConfig
)(ProfileModal);

const mapStateToProps = (state) => ({
    showModalProfile: connectors.getOpen(state),
    initialValues: connectors.getPeluqueria(state)
});

ProfileModal = connect(
    mapStateToProps,
    actions
)(ProfileModal);

export default ProfileModal;