import React, { Component } from 'react';
import { Field } from 'redux-form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Alert from 'react-bootstrap/lib/Alert';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import renderField from './Validation/RenderField';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';
import './Login.css';

class Login extends Component {

    doLoginIfEnter(e, email, password) {
        if (e.keyCode == 13) {
            this.doLogin(email, password);
        }
    }

    doLogin(email, password) {
        const { login } = this.props;
        login(email.value.trim(), password.value.trim());
    }

    hideAlert() {
        const { hideInvalidCredentials } = this.props;
        hideInvalidCredentials();
    }

    forgotPassword() {
        const { forgotPassword } = this.props;
        forgotPassword();
    }

    createAccount() {
        const { createAccount } = this.props;
        createAccount();
    }

    render() {
        const { handleSubmit, invalidCredentials, error } = this.props;
        let email, password = "";
        let msgInvalidCredentials = "";
        if (invalidCredentials) {
            msgInvalidCredentials = (
                <Alert bsStyle="danger" onDismiss={() => this.hideAlert()}>
                    <p>{formatMessage(messages.credencialesInvalidas)}</p>
                </Alert>
            );
        } else if (error) {
            msgInvalidCredentials = (
                <Alert bsStyle="danger" onDismiss={() => this.hideAlert()}>
                    <p>{formatMessage(messages.errorGenerico)}</p>
                </Alert>
            );
        }
        return (
            <div>
                <FormGroup>
                    <div className="form-group">
                        <input placeholder={formatMessage(messages.email)} ref={node => email = node} className="form-control" type="text" />
                    </div>
                    <div className="form-group">
                        <input
                            placeholder={formatMessage(messages.password)}
                            ref={node => password = node}
                            className="form-control"
                            type="password"
                            onKeyUp={e => this.doLoginIfEnter(e, email, password)}
                        />
                    </div>
                    <Button bsStyle="link" className="Login-forgot" onClick={() => this.forgotPassword()}>
                        {formatMessage(messages.olvidastePassword)}
                    </Button>
                </FormGroup>
                {msgInvalidCredentials}
                <Button bsStyle="success" className="Login-button" type="button" onClick={() => this.doLogin(email, password)} >
                    {formatMessage(messages.entrar)}
                </Button>
                <Button bsStyle="link" className="Login-signup" onClick={() => this.createAccount()}>
                    {formatMessage(messages.crearCuenta)}
                </Button>
            </div>
        );
    }
}

export default Login;