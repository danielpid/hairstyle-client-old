import { formatMessage, formatDate } from './../../../i18n';
import messages from './../../../i18n/messages';

export const views = ['month', 'week', 'day', 'agenda'];

export const messagesCalendar = {
    allDay: formatMessage(messages.todoElDia),
    previous: '<',
    next: '>',
    today: formatMessage(messages.hoy),
    month: formatMessage(messages.mes),
    week: formatMessage(messages.semana),
    day: formatMessage(messages.dia),
    agenda: formatMessage(messages.agenda)
};

export const formats = {
    timeGutterFormat: 'HH:mm',
    dayFormat: 'ddd D',
    dayRangeHeaderFormat: ({ start, end }) =>
        getMonth(start) + ' ' + getYear(start),
    dayHeaderFormat: (date) =>
        getDayOfTheWeek(date) + ' ' + getDayOfTheMonth(date) + ' ' + getMonth(date),
    monthHeaderFormat: (date) =>
        getMonth(date) + ' ' + getYear(date),
    selectRangeFormat: ({ start, end }) =>
        getHourMinute24(start) + '-' + getHourMinute24(end),
    agendaTimeRangeFormat: ({ start, end }) =>
        getHourMinute24(start) + '-' + getHourMinute24(end),
    eventTimeRangeFormat: ({ start, end }) =>
        getHourMinute24(start) + '-' + getHourMinute24(end)
};

const getHourMinute24 = (date) =>
    formatDate(date, { hour12: false, hour: 'numeric', minute: 'numeric' })

const getDayOfTheWeek = (date) => {
    const diaSemana = formatDate(date, { weekday: 'long' });
    return diaSemana.charAt(0).toUpperCase() + diaSemana.slice(1);
}

const getDayOfTheMonth = (date) =>
    formatDate(date, { day: 'numeric' });

const getMonth = (date) => {
    const mes = formatDate(date, { month: 'long' })
    return mes.charAt(0).toUpperCase() + mes.slice(1);
}

const getYear = (date) =>
    formatDate(date, { year: 'numeric' })