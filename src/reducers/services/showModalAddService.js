const showModalAddService = (state = false, action) => {
    switch (action.type) {
        case 'OPEN_MODAL_ADD_SERVICE':
            return true;
        case 'CLOSE_MODAL_ADD_SERVICE':
            return false;
        default:
            return state;
    }
}

export default showModalAddService;