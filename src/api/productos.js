import fetch from 'isomorphic-fetch';
import * as api from './common';

const url = api.urlBase + '/api/productos';

export const fetchProductos = () =>
    api.fetchAll(url);

export const fetchProductosByCriteria = (nombre, precio, stock) =>
    api.fetchByCriteria(url, { nombre, precio, stock });

export const addProducto = (nombre, precio, stock) =>
    api.add(url, { nombre, precio, stock });

export const editProducto = (idProducto, nombre, precio, stock) =>
    api.edit(url, { idProducto, nombre, precio, stock });

export const removeProductos = (idsProducto) =>
    api.remove(url, idsProducto);