import React from 'react';
import Table from 'react-bootstrap/lib/Table';
import messages from './../../../i18n/messages';
import { formatMessage } from './../../../i18n';
import './LineaFacturaTable.css';

const LineaFacturaTable = ({factura}) => (
    <Table striped bordered condensed hover className="LineaFacturaTable">
        <thead>
            <tr>
                <th>{formatMessage(messages.descripcion)}</th>
                <th>{formatMessage(messages.cantidad)}</th>
                <th>{formatMessage(messages.precioUnidad)}</th>
                <th>{formatMessage(messages.precio)}</th>
            </tr>
        </thead>
        <tbody>
            {
                factura.detalle.map(lineaFactura =>
                    <tr key={lineaFactura.idLineaFactura + 'LF'}>
                        <td key={lineaFactura.idLineaFactura + 'Nombre'}>{lineaFactura.nombreServicio || lineaFactura.nombreProducto}</td>
                        <td key={lineaFactura.idLineaFactura + 'Ctdad'}>{lineaFactura.cantidad}</td>
                        <td key={lineaFactura.idLineaFactura + 'Unidad'}>{lineaFactura.precioUnidad} €</td>
                        <td key={lineaFactura.idLineaFactura + 'Precio'}>{lineaFactura.precioLineaFactura} €</td>
                    </tr>
                )
            }
        </tbody>
    </Table>
);

export default LineaFacturaTable;