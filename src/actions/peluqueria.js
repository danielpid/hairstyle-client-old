import { normalize } from 'normalizr';
import * as schema from '../api/schema';
import * as api from '../api/peluqueria';
import * as connector from '../reducers/peluqueria/peluqueria';
import alert from './alert';
import messages from './../i18n/messages';
import { formatMessage } from './../i18n';
import { handleError } from './common';
import moment from 'moment';
import { login } from './login';

export const fetchPeluqueria = () => (dispatch, getState) => {
    if (connector.getIsFetching(getState())) {
        return Promise.resolve();
    }
    dispatch({
        type: 'FETCH_PELUQUERIA_REQUEST'
    });
    return api.fetchPeluqueria().then(
        response => {
            response.horaApertura = moment(response.horaApertura, 'HH:mm');
            response.horaCierre = moment(response.horaCierre, 'HH:mm');
            dispatch({
                type: 'FETCH_PELUQUERIA_SUCCESS',
                response
            });
        },
        error => {
            handleError(dispatch, error, 'FETCH_PELUQUERIA_FAILURE');
        }
    );
};

export const addPeluqueria = (nombre, email, horaApertura, horaCierre, passwordActual, passwordNueva) =>
    (dispatch, getState) => {
        if (connector.getIsAdding(getState())) {
            return Promise.resolve();
        }
        dispatch({
            type: 'ADD_PELUQUERIA_REQUEST'
        });
        api.addPeluqueria(nombre, email, horaApertura, horaCierre, passwordActual, passwordNueva).then(
            response => {
                dispatch({
                    type: 'ADD_PELUQUERIA_SUCCESS'
                });
                login(email, passwordNueva)(dispatch, getState);
            },
            error => {
                handleError(dispatch, error, 'ADD_PELUQUERIA_FAILURE');
            }
        );
    };

export const editPeluqueria = (nombre, email, horaApertura, horaCierre, passwordActual, passwordNueva, message,
    callback) => (dispatch, getState) => {
        if (connector.getIsEditing(getState())) {
            return Promise.resolve();
        }
        dispatch({
            type: 'EDIT_PELUQUERIA_REQUEST'
        });
        api.editPeluqueria(nombre, email, horaApertura, horaCierre, passwordActual, passwordNueva).then(
            response => {
                response.horaApertura = moment(response.horaApertura);
                response.horaCierre = moment(response.horaCierre);
                message = message || messages.informacionActualizada;
                alert('success', formatMessage(message));
                dispatch({
                    type: 'EDIT_PELUQUERIA_SUCCESS',
                    response
                });
                if (callback) {
                    callback();
                }
            },
            error => {
                if (error.message == 401) {
                    alert('error', formatMessage(messages.passwordIncorrecta));
                    dispatch({
                        type: 'EDIT_PELUQUERIA_FAILURE'
                    });
                } else {
                    handleError(dispatch, error, 'EDIT_PELUQUERIA_FAILURE');
                }
            }
        );
    };

export const resetUserPassword = () => {

}

export const openModalProfile = (peluqueria) => ({
    type: "OPEN_MODAL_PROFILE",
    peluqueria
});

export const closeModalProfile = (peluqueria) => ({
    type: "CLOSE_MODAL_PROFILE"
});

export const createAccount = () => (dispatch) => (
    dispatch({
        type: 'CREATE_ACCOUNT'
    })
)